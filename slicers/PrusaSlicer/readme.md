# Launchers
## Windows
1. create launcher to PrusaSlicer executable
2. Edit the shortcut target to look like this:
```
"C:\Program Files\Prusa3D\PrusaSlicer\prusa-slicer.exe" --datadir "C:\Users\aaron\Documents\GitHub\ultimate-xl-config\slicers\PrusaSlicer"
```